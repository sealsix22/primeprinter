
Aplikace slouzi vypis vsech prvocisel z excel souboru jehoz cesta je predana jako commandline parametr programu.

- Aplikace se sestavuje mavenem (mvn clean package), pri buildu se automaticky pusti testy a po buildu je v target adresari spustitelny jar soubor.

Priklad pusteni na Windows:
 ```
java -jar PrimePrinter-1.0.0.jar "C:\sample data.xlsx"
```

#### dulezite poznamky:
- protoze vstupni soubor muze byt velky, aplikace byla implementovana pomoci Apache POI event API misto standardniho Apache POI api tak, aby nepotrebovala
nacist cely soubor do pameti, ale zpracovava kazdy element excel souboru samostatne.
- pokud by bylo potreba, je mozne trochu navysit pamet napr -Xms50m -Xmx50m


#### TODO list:
- otestovat na velkych souborech
- opravit zakomentovany test testsample2 v PrimePrinterApplicatioTest. Kdyz upravim vas vstupni excel soubor, muj program 
v nem nenajde zadne data. Zkousel jsem i Google Tabulky a export, ale se stejnym vysledkem. Pravdepodobne nemam excel ve spravnem
formatu.
- pridat vice testu
- pridat releasovani napr pres maven-release-plugin

