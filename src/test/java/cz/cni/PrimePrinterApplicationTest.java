package cz.cni;

import cz.cni.helper.TestRunner;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PrimePrinterApplicationTest {

    private TestRunner runner;

    private final String sampledataPath = "src/test/resources/";

    @BeforeEach
    public void initRunner() {
        runner = new TestRunner();
    }

    @Test
    public void testSample() throws Exception {
        runner.mainArgs(sampledataPath + "sampledata.xlsx")
                .results("5645657", "15619", "1234187", "211", "7", "9788677", "23311", "54881")
                .run();
    }

    //@Test
    public void testSample2() throws Exception {
        runner.mainArgs(sampledataPath + "sampledata2.xlsx")
                .results("5645657", "15619", "1234187", "211", "7", "9788677", "23311", "54881")
                .run();
    }

    @Test
    public void testWrongFile() {
        assertThrows(InvalidOperationException.class, () -> runner.mainArgs("nic").run());
    }

    @Test
    public void testWrongFile2() {
        assertThrows(IllegalArgumentException.class, () -> runner.run());
    }

}
