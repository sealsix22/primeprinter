package cz.cni.helper;

import cz.cni.PrimePrinterApplication;
import org.junit.jupiter.api.Assertions;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author Petr.Mensik
 *
 * Helper builder class for prepare test input, run app and check output
 */
public class TestRunner {

    private final PrintStream systemOut = System.out;

    private String[] args = new String[0];
    private final List<String> results = new LinkedList<>();

    public TestRunner results(String... result) {
        results.addAll(Arrays.asList(result));
        return this;
    }

    public TestRunner mainArgs(String... arg) {
        this.args = arg;
        return this;
    }

    public void run() throws Exception {
        try(ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream();
            PrintStream testPrintStream = new PrintStream(testOutputStream)) {

            System.setOut(testPrintStream);

            PrimePrinterApplication.main(args);

            testPrintStream.flush();
            final String output = getOutput(testOutputStream);
            List<String> outputRows = new LinkedList<>();
            if(output != null && !output.isEmpty()) {
                outputRows =  Arrays.asList(output.split(System.lineSeparator()));
            }
            outputRows.sort(Comparator.naturalOrder());
            results.sort(Comparator.naturalOrder());

            Assertions.assertTrue(results.containsAll(outputRows));
            Assertions.assertTrue(outputRows.containsAll(results));
        } finally {
            System.setOut(systemOut);
        }
    }

    private String getOutput(final ByteArrayOutputStream outputStream) {
        try {
            return outputStream.toString(StandardCharsets.UTF_8.toString()).trim();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
