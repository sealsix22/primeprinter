package cz.cni;

/**
 * Processing of cell values
 */
public interface ICellValueProcessor {

    /**
     * Process cell value
     * @param cellValue value param
     */
    void processCellValue(String cellValue);
}
