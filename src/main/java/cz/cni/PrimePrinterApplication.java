package cz.cni;

import java.io.InputStream;

import org.apache.poi.util.XMLHelper;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Opens excel file on path passed as first commandline argument, parses second column on first sheet
 * and prints all prime numbers.
 * Please see Readme.md
 */
public class PrimePrinterApplication {

    public static final String FIRST_SHEET_ID = "rId2";

    public static void main(String[] args) throws Exception {
        processXlsxFile(getFilePath(args));
    }

    /**
     * get first command line argument as file path
     * @param args main method arguments
     * @return file path from command line
     */
    private static String getFilePath(final String[] args) {
        if (args.length == 0 || args[0] == null || args[0].isEmpty()) {
            throw new IllegalArgumentException("first argument has to be valid openable excel xls file");
        }

        return args[0];
    }

    /**
     * reads Xlsx file, opens first sheet and process it
     * @param filename filename
     * @throws Exception exceptions when processing file
     */
    private static void processXlsxFile(final String filename) throws Exception {
        try (final OPCPackage pkg = OPCPackage.open(filename)) {
            final XSSFReader r = new XSSFReader(pkg);
            try (final SharedStringsTable sst = r.getSharedStringsTable();
                 final InputStream sheet = r.getSheet(FIRST_SHEET_ID)) {

                final XMLReader parser = createXMLReader(sst);
                parser.parse(new InputSource(sheet));
            }
        }
    }

    /**
     * Creates @{@link XMLReader processing XLSX file and sets its sheet handler @DefaultHandler
     * and cell value processor @link ICellValueProcessor}
     * @param sharedStringsTable
     * @return created @{@link XMLReader}
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    private static XMLReader createXMLReader(final SharedStringsTable sharedStringsTable)
            throws SAXException, ParserConfigurationException {
        final XMLReader xmlReader = XMLHelper.newXMLReader();
        final ContentHandler handler = new StringValuesSheetHandler(sharedStringsTable, new CellValueProcessor());
        xmlReader.setContentHandler(handler);

        return xmlReader;
    }
}


