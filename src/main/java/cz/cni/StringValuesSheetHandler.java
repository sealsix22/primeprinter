package cz.cni;

import org.apache.poi.xssf.model.SharedStringsTable;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Objects;

/**
 * Handler passes string cell values to @{@link CellValueProcessor}
 * See org.xml.sax.helpers.DefaultHandler javadocs
 */
public class StringValuesSheetHandler extends DefaultHandler {

    public static final String CELL_NAME = "c";
    public static final String Q_NAME = "t";
    public static final String STRING_CELL_TYPE = "s";
    public static final String CELL_VALUE_NAME = "v";

    private final SharedStringsTable sharedStringsTable;
    private final ICellValueProcessor cellValueProcessor;

    private String lastContents = "";
    private boolean nextIsString;

    public StringValuesSheetHandler(SharedStringsTable sharedStringsTable, ICellValueProcessor cellValueProcessor) {
        Objects.requireNonNull(sharedStringsTable);
        Objects.requireNonNull(cellValueProcessor);

        this.sharedStringsTable = sharedStringsTable;
        this.cellValueProcessor = cellValueProcessor;
    }

    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes) {
        if (CELL_NAME.equals(name)) {
            // Figure out if the value is an index in the SST
            final String cellType = attributes.getValue(Q_NAME);
            nextIsString = STRING_CELL_TYPE.equals(cellType);
        }
        // Clear contents cache
        lastContents = "";
    }

    @Override
    public void endElement(String uri, String localName, String name) {
        // Process the last contents as required.
        // Do now, as characters() may be called more than once
        if (nextIsString) {
            int idx = Integer.parseInt(lastContents);
            lastContents = sharedStringsTable.getItemAt(idx).getString();
            nextIsString = false;
        }
        // v => contents of a cell
        // Output after we've seen the string contents
        if (CELL_VALUE_NAME.equals(name)) {
            cellValueProcessor.processCellValue(lastContents);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        lastContents += new String(ch, start, length);
    }
}
