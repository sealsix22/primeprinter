package cz.cni;

import java.util.stream.IntStream;

/**
 * Default implementation which prints all prime number values to System out
 */
public class CellValueProcessor implements ICellValueProcessor {

    @Override
    public void processCellValue(String cellValue) {
        if(isPrime(cellValue)) {
            System.out.println(cellValue);
        }
    }

    /**
     * check value whether is prime number
     * @param value input value
     * @return true if and only if value is prime number
     */
    private boolean isPrime(final String value) {
        try {
            final Integer number = Integer.valueOf(value);
            return number != null && !IntStream.rangeClosed(2, number / 2).anyMatch(i -> number % i == 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
